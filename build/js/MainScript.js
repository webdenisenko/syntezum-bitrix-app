let button = document.getElementById('submit');

let textarea = document.getElementById('data');

let ManageTable = new ManageTable('learn_table');

button.addEventListener('click', function (e) {
    e.preventDefault();

    let value = textarea.value.trim();

    if(value === '') return alert('Вы не вставили данные.');

    let data = [];
    let date = (new Date()).toISOString();

    let log = document.getElementById('log');
    let body = document.getElementsByTagName('body')[0];

    body.classList = 'start';

    let lines = value.split('\n');
    for(let l in lines){
        let item = lines[l].split('\t');

        if(!(item && item[0] && item[1] && item[2] && item[11] && item[16] && item[17])){
            log.innerHTML += 'Строка #'+l+' некорректная.<br>';
            continue;
        }

        data.push({
            'NAME': item[0].trim(),
            'FIO': item[1].trim(),
            'GROUP': item[2].trim(),
            'REG_ID': item[11].trim(),
            'BALANCE': item[16].trim(),
            'CREDIT': item[17].trim(),
            // 'REC_DateTime': date
        });
    }

    let processed = 0;

    ManageTable.InsertOrUpdate(data, ['NAME'], function (item, status) {
        processed++;

        switch (status){

            case 'same':
                status = 'без изменений';
                break;

            case 'inserted':
                status = 'успешно добавлено';
                break;

            case 'updated':
                status = 'успешно обновлено';
                break;

            default:
                status = 'произошла ошибка ('+ status.replace('<br>', '') +')';
                break;

        }

        log.innerHTML += '<b>' + item.FIO + '(' + item.NAME + ')</b> - '+ status +'.<br>';

        if(processed === data.length) log.innerHTML += '<b style="color:#0F0">Завершено! Всего обработано '+data.length+' строк.</b><br>';
    });
});