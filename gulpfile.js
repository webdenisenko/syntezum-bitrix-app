const gulp = require('gulp'),    // Preprocessors
    babel = require('gulp-babel'),    // Error notifications
    gutil = require('gulp-util'),    // AutoReloader
    ftp = require( 'vinyl-ftp' ),
    browserSync = require('browser-sync').create(),    // For production
    concat = require('gulp-concat'),
    inlinesource = require('gulp-inline-source'),
    fs = require('fs'),
    uglify = require('gulp-uglify');


var
    ES6toES5 = true,
    path = {
        'source': {
            'js': 'build/js',
            'pages': 'build'
        },
        'dist':{
            'js': 'dist/js',
            'pages': 'dist'
        }
    },
    ftp_settings = {
        'user': 'admin_stz',
        'password': '33bRIDI9E6',
        'host': 'syntezum.webden-studio.com',
        'port': 21,
        'rootFolder': '/'
    };

// helper function to build an FTP connection based on our configuration
function getFtpConnection() {
    return ftp.create({
        host: ftp_settings.host,
        port: ftp_settings.port,
        user: ftp_settings.user,
        password: ftp_settings.password,
        parallel: 5
    });
}

// Preprocessor ES6 to ES5 with `browserify`
function JSError(err) {
    const message = err.message || '';
    const errName = err.name || '';
    const codeFrame = err.codeFrame || '';
    gutil.log(gutil.colors.red.bold('[JS babel error]')+' '+ gutil.colors.bgRed(errName));
    gutil.log(gutil.colors.bold('message:') +' '+ message);
    gutil.log(gutil.colors.bold('codeframe:') + '\n' + codeFrame);
    this.emit('end');
}
gulp.task('js', function() {
    return gulp.src(path.source.js + '/**/*.js')
        .pipe(
            babel({
                presets: ['es2015']
            }).on('error', JSError)
        )
        .pipe(gulp.dest(path.dist.js));
});

// HTML optimization
gulp.task('html', ['js'], function() {
    if( ! path.source.pages){
        gutil.log(gutil.colors.red.bold('Warning:') + ' Pages path is not set');
        return this.emit('end');
    }

    gulp.src(path.source.pages + '/**/*.+(html|php)')
        .pipe(gulp.dest(path.dist.pages))
        .pipe(browserSync.stream());

    gutil.log(gutil.colors.green.bold('Success:') + ' HTML optimization');
    return this.emit('end');

});

// LiveReload server
gulp.task('browser-sync', function() {
    return browserSync.init({
        open: false,
        notify: false,
        server: {baseDir:'./dist'}
    });
});

// AutoSave JS and LiveReload
gulp.task('watch', ['browser-sync', 'js', 'html'], function() {

    // ES6 to ES5 preprocessor
    if(ES6toES5) gulp.watch(path.source.js+'/**/*.js', ['html']);

    // JS watch
    if(path.source.js) gulp.watch(path.source.js+'/**/*.js', browserSync.reload);

    // HTML/PHP watch
    if(path.source.pages) gulp.watch(path.source.pages+'/**/*.+(php|html)', ['html']);

    var conn = getFtpConnection();

    gulp.watch(path.dist.pages+'/js/**')
        .on('change', function(event) {
            gutil.log(gutil.colors.green.bold('Changes detected: ') + 'Uploading file "' + event.path + '", ' + event.type);

            return gulp.src( [event.path], { base: 'dist', buffer: false } )
                .pipe( conn.dest( ftp_settings.rootFolder ) );
        });

});

// JS optimization
gulp.task('o', ['js'], function() {
    if( ! path.source.js){
        gutil.log(gutil.colors.red.bold('Warning:') + ' JS path is not set');
        return this.emit('end');
    }

    gulp.src(path.dist.js+'/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(path.dist.js));

    return gulp.src(path.source.pages + '/**/*.+(html|php)')
        .pipe(inlinesource({
            rootpath: path.dist.pages
        }))
        .pipe(gulp.dest(path.dist.pages));

    gutil.log(gutil.colors.green.bold('Success:') + ' JS optimization');
    return this.emit('end');
});

gulp.task('default', ['watch']);