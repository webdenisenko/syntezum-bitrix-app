<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bitrix24 | App</title>
    <script src="//api.bitrix24.com/api/v1/"></script>
    <style>
        #log{
            display: none;
            background: #EEE;
            border: 1px solid;
            color: #AAA;
            padding: 15px;
            height: 360px;
            overflow: auto;
        }

        #refresh{
            display: none;
            margin-left: 15px;
        }

        .start #log{
            display: block;
        }

        .start #refresh{
            display: inline-block;
        }
    </style>
</head>
<body>
    <textarea cols="80" rows="20" id="data" placeholder="Вставьте данные сюда...">
        12022048	Алексей Паламарчук	NI-fx-m-cfd-au	Украина	Киев		02097	ул. Радуская, 9, кв. 85	+383806744561	padi@ukr.net	NT - hedge	08c384ca	1	2015.11.25 16:00	1 : 100	 	4 589.04	4 813.00	0.00	0.00	441B4DA3	NT - hedge
        12022551	Роман Басовский	NI-fx-m-cfd-au	Украина	Хмельницкий		29000	ул. Шевченка 99, кв.36	+380671791068	191roman@gmail.com	NT - hedge	3eb81ea0	1	2016.09.19 10:15	1 : 100	 	1 458.71	1 500.00	0.00	0.00	 	NT - hedge
        12022560	Игорь Корнелюк	NI-fx-m-cfd-au	Украина	Ковель		45000	ул. Ст.Бандеры , 24	+380951912885	ikornelyuk101@gmail.com	NT - hedge	067a5c71	1	2016.09.22 17:22	1 : 100	 	798.27	1 000.00	0.00	0.00	 	NT - hedge
        12022608	Yuriy Vytrykush	NI-fx-m-cfd-au	Украина	Львов		79013	вул. Глибока 13 кв 3	+380934624904	Yurik.vytrykush@gmail.com	NT - hedge	f848528a	1	2016.10.07 13:13	1 : 100	 	941.67	1 003.15	0.00	0.00	 	NT - hedge
        12022622	Олексій Кушнірук	NI-fx-m-cfd-au	Украина	Ровно		33005	Бориса Шведа 6, кв2.	+38380961130146	olk.v333@gmail.com	NT - hedge	e54ff051	1	2016.10.12 11:43	1 : 100	 	1 897.52	1 948.60	0.00	0.00	 	NT - hedge
        12022645	Дмитрий Марчук	NI-fx-m-cfd-au	Украина	Ивано-Франковск		76002	вул. Гната Хоткевича 71,41	+380992253753	marchukdmytro76@gmail.com	NT - hedge	226866db	1	2016.10.18 16:22	1 : 100	 	4 665.56	5 000.00	0.00	0.00	 	NT - hedge
        12022650	Володимир-Любомир Білик	NI-fx-m-cfd-au	Украина	Львов		79491	смт. Брюховичі вул. Соснова 12	+380937290711	vovabiluk1996@gmail.com	DEMO WEB	709f69e3	1	2016.10.20 17:10	1 : 100	 	5 808.75	615.00	0.00	0.00
        12022653	Антон Корунов	NI-fx-m-cfd-au	Украина	Ровно		33000	Волинської Дивізії 31 кв.9	+380978944775	to4ka9@mail.ru	NT - hedge	78eabedc	1	2016.10.24 15:12	1 : 100	 	5 538.80	10 000.00	0.00	0.00	817828D4	NT - hedge
    </textarea>
    <br><br><input id="submit" type="submit" value="Готово"><input onclick="location.reload()" id="refresh" type="submit" value="Очистить"><br><br>

    <div id="log"></div>

    <script inline src="js/ManageTable.js"></script>
    <script inline src="js/MainScript.js"></script>
</body>
</html>