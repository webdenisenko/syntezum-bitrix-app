'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var ManageTable = function () {
    function ManageTable(tableName) {
        _classCallCheck(this, ManageTable);

        this.tableName = tableName;
        this.loaded = false;
        this.changed = false;
        this.callbackFunction = function () {};
    }

    _createClass(ManageTable, [{
        key: 'Init',
        value: function Init() {
            var callbackFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};

            this.callbackFunction = callbackFunction;
            this.GetTable();
        }
    }, {
        key: 'Query',
        value: function Query(config) {
            if (!config['action']) return console.error('Query action is not set.', config);

            var action = config['action'];

            var param = {
                'IBLOCK_TYPE_ID': 'bitrix_processes'
            };

            var success = function success(result) {
                console.log('Success query:', result);
            };

            var error = function error(result) {
                alert("Ошибка: " + result.error());
            };

            if (config['IBLOCK_TYPE_ID']) param['IBLOCK_TYPE_ID'] = config['IBLOCK_TYPE_ID'];
            if (config['IBLOCK_CODE']) param['IBLOCK_CODE'] = config['IBLOCK_CODE'];
            if (config['ELEMENT_CODE']) param['ELEMENT_CODE'] = config['ELEMENT_CODE'];
            if (config['LIST_ELEMENT_URL']) param['LIST_ELEMENT_URL'] = config['LIST_ELEMENT_URL'];
            if (config['FIELDS']) param['FIELDS'] = config['FIELDS'];

            if (config['success']) success = config['success'];
            if (config['error']) error = config['error'];

            console.info(action, param);

            BX24.callMethod(action, param, function (result) {
                return result.error() ? error(result) : success(result);
            });
        }
    }, {
        key: 'GetTable',
        value: function GetTable() {
            var _this = this;
            var name = _this.tableName;

            _this.Query({
                'action': 'lists.get',
                'IBLOCK_CODE': name,
                'success': function success(result) {
                    // Return alert if table is not exists
                    if (!result.data().length) return alert('Таблицы ' + name + ' не существует.');

                    // Save table data
                    _this.table = result.data()[0];
                    _this.GetFields();
                }
            });
        }
    }, {
        key: 'GetFields',
        value: function GetFields() {
            var _this = this;

            var tableCode = _this.table.CODE;

            _this.Query({
                'action': 'lists.field.get',
                'IBLOCK_CODE': tableCode,
                'success': function success(result) {
                    result = result.data();

                    // Return alert if table fields is not exists
                    if (!result || !result['NAME']) return alert('Таблица ' + _this.table.NAME + ' не имеет полей.');

                    // Save table fields data
                    _this.tableFields = result;
                    _this.GetFieldsData(_this.callbackFunction);
                    _this.callbackFunction = function () {};
                }
            });
        }
    }, {
        key: 'GetFieldsData',
        value: function GetFieldsData() {
            var callbackFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : function () {};

            var _this = this;

            _this.Query({
                'action': 'lists.element.get',
                'IBLOCK_CODE': _this.tableName,
                'success': function success(result) {
                    result = result.data();

                    // Save table fields data
                    _this.sourceFieldsData = result;

                    var beautyData = [];

                    var _loop = function _loop(itemIndex) {
                        var item = result[itemIndex];

                        beautyData[item['NAME']] = {};
                        beautyData[item['NAME']]['_source_key'] = itemIndex;

                        var _loop2 = function _loop2(key) {
                            // Create element in object by key
                            if (_this.tableFields[key]['MULTIPLE'] == 'N') {

                                // CODE of key
                                var code = _this.tableFields[key].CODE ? _this.tableFields[key].CODE : key;

                                // Save first key from object
                                if (_typeof(item[key]) === 'object') {
                                    Object.keys(item[key]).forEach(function (n) {
                                        beautyData[item['NAME']][code] = item[key][n];
                                    });
                                }

                                // Just save value
                                else {
                                        beautyData[item['NAME']][code] = item[key];
                                    }
                            } else {
                                beautyData[item['NAME']][key] = item[key];
                            }
                        };

                        for (var key in _this.tableFields) {
                            _loop2(key);
                        }
                    };

                    for (var itemIndex in result) {
                        _loop(itemIndex);
                    }
                    _this.beautyFieldsData = beautyData;
                    _this.loaded = true;
                    _this.changed = false;
                    callbackFunction(_this);

                    console.log(_this);
                }
            });
        }
    }, {
        key: 'BeautyToSource',
        value: function BeautyToSource(item) {
            var valueKeys = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            var _this = this;
            var sourceItem = {};

            // Each item field
            Object.keys(item).forEach(function (itemFieldName) {
                if (itemFieldName == 'NAME') {
                    sourceItem.NAME = item.NAME;
                } else {
                    // Each source field
                    Object.keys(_this.tableFields).forEach(function (property_id) {
                        // Matching CODEs
                        if (_this.tableFields[property_id].CODE == itemFieldName) {
                            if (valueKeys) {
                                if (!item._source_key) return console.error('Item _source_key not found');

                                var firstKey = typeof _this.sourceFieldsData[item._source_key][property_id] !== 'undefined' ? Object.keys(_this.sourceFieldsData[item._source_key][property_id])[0] : 1;

                                if (!sourceItem[property_id]) {
                                    sourceItem[property_id] = {};
                                }

                                sourceItem[property_id][firstKey] = item[itemFieldName];
                            } else {
                                sourceItem[property_id] = item[itemFieldName];
                            }
                        }
                    });
                }
            });

            return sourceItem;
        }
    }, {
        key: 'Slug',
        value: function Slug(str) {
            var letters = [['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'], ['д', 'd'], ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'], ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'], ['м', 'm'], ['н', 'n'], ['о', 'o'], ['п', 'p'], ['р', 'r'], ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'], ['х', 'h'], ['ц', 'c'], ['ч', 'ch'], ['ш', 'sh'], ['щ', 'shch'], ['ъ', ''], ['ы', 'y'], ['ь', ''], ['э', 'e'], ['ю', 'yu'], ['я', 'ya'], ['А', 'A'], ['Б', 'B'], ['В', 'V'], ['Г', 'G'], ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'], ['Ж', 'ZH'], ['З', 'Z'], ['И', 'I'], ['Й', 'Y'], ['К', 'K'], ['Л', 'L'], ['М', 'M'], ['Н', 'N'], ['О', 'O'], ['П', 'P'], ['Р', 'R'], ['С', 'S'], ['Т', 'T'], ['У', 'U'], ['Ф', 'F'], ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'], ['Ъ', ''], ['Ы', 'Y'], ['Ь', ''], ['Э', 'E'], ['Ю', 'YU'], ['Я', 'YA'], ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'], ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'], ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'], ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'], ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'], ['z', 'z'], ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'], ['E', 'E'], ['F', 'F'], ['G', 'G'], ['H', 'H'], ['I', 'I'], ['J', 'J'], ['K', 'K'], ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'], ['P', 'P'], ['Q', 'Q'], ['R', 'R'], ['S', 'S'], ['T', 'T'], ['U', 'U'], ['V', 'V'], ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'], [' ', '_'], ['0', '0'], ['1', '1'], ['2', '2'], ['3', '3'], ['4', '4'], ['5', '5'], ['6', '6'], ['7', '7'], ['8', '8'], ['9', '9'], ['-', '-']];

            var clearString = '';

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = str[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var ch = _step.value;

                    var newCh = '';
                    var _iteratorNormalCompletion2 = true;
                    var _didIteratorError2 = false;
                    var _iteratorError2 = undefined;

                    try {
                        for (var _iterator2 = letters[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                            var j = _step2.value;

                            if (ch == j[0]) {
                                newCh = j[1];
                            }
                        }
                    } catch (err) {
                        _didIteratorError2 = true;
                        _iteratorError2 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion2 && _iterator2.return) {
                                _iterator2.return();
                            }
                        } finally {
                            if (_didIteratorError2) {
                                throw _iteratorError2;
                            }
                        }
                    }

                    clearString += newCh;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return clearString.toString().replace(/\s+/g, '_') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
        }
    }, {
        key: 'Get',
        value: function Get() {
            var searchParams = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

            var _this = this;
            var data = [];
            var paramsIsEmpty = true;

            if (_this.changed) {
                _this.GetFieldsData(function () {
                    _this.Get(searchParams);
                });
            } else {
                if (_this.loaded) {
                    // Each data item
                    Object.keys(_this.beautyFieldsData).forEach(function (item) {
                        item = _this.beautyFieldsData[item];

                        var pushed = false;
                        // Each search parameter
                        Object.keys(searchParams).forEach(function (paramIndex) {
                            paramsIsEmpty = false;
                            if (!pushed && item[paramIndex] == searchParams[paramIndex]) {
                                data.push(item);
                                pushed = true;
                            }
                        });
                    });

                    if (paramsIsEmpty) data = _this.beautyFieldsData;

                    return data;
                } else {
                    _this.Init(function () {
                        _this.Get(searchParams);
                    });
                }
            }
        }
    }, {
        key: 'Insert',
        value: function Insert(item, code) {
            var callBackFunction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

            var _this = this;

            var source_item = _this.BeautyToSource(item);

            if (_this.loaded) {
                return _this.Query({
                    'action': 'lists.element.add',
                    'IBLOCK_CODE': _this.tableName,
                    'ELEMENT_CODE': code,
                    'FIELDS': source_item,
                    success: function success() {
                        callBackFunction(item, 'inserted');
                        _this.changed = true;
                    },
                    error: function error(_error) {
                        callBackFunction(item, _error.answer.error + ': ' + _error.answer.error_description);
                    }
                });
            } else {
                _this.Init(function () {
                    _this.Insert(item, code, callBackFunction);
                });
            }
        }
    }, {
        key: 'Update',
        value: function Update(item, code) {
            var callBackFunction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

            var _this = this;

            var source_item = _this.BeautyToSource(item, true);

            if (_this.loaded) {
                return _this.Query({
                    'action': 'lists.element.update',
                    'IBLOCK_CODE': _this.tableName,
                    'ELEMENT_CODE': code,
                    'FIELDS': source_item,
                    success: function success() {
                        callBackFunction(item, 'updated');
                        _this.changed = true;
                    },
                    error: function error(_error2) {
                        callBackFunction(item, _error2.answer.error + ': ' + _error2.answer.error_description);
                    }
                });
            } else {
                _this.Init(function () {
                    _this.Update(item, code, callBackFunction);
                });
            }
        }
    }, {
        key: 'InsertOrUpdate',
        value: function InsertOrUpdate(data, fields) {
            var callBackFunction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : function () {};

            var _this = this;

            if (_this.loaded) {
                for (var itemKey in data) {
                    var _item = data[itemKey];

                    // Formatting search parameters from source
                    var searchParams = {};
                    var _iteratorNormalCompletion3 = true;
                    var _didIteratorError3 = false;
                    var _iteratorError3 = undefined;

                    try {
                        for (var _iterator3 = fields[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                            var field = _step3.value;

                            searchParams[field] = _item[field];
                        }

                        // Check items with current search parameters
                    } catch (err) {
                        _didIteratorError3 = true;
                        _iteratorError3 = err;
                    } finally {
                        try {
                            if (!_iteratorNormalCompletion3 && _iterator3.return) {
                                _iterator3.return();
                            }
                        } finally {
                            if (_didIteratorError3) {
                                throw _iteratorError3;
                            }
                        }
                    }

                    var dbItems = this.Get(searchParams);

                    var programCallBackFunction = function programCallBackFunction(item, status) {
                        return callBackFunction(item, status);
                    };

                    if (itemKey >= data.length - 1) {
                        programCallBackFunction = function programCallBackFunction(item, status) {
                            return _this.GetFieldsData(callBackFunction(item, status));
                        };
                    }

                    // Update data if items is exists
                    if (dbItems.length) {
                        _item._source_key = dbItems[0]._source_key;
                        this.Update(_item, _this.sourceFieldsData[dbItems[0]._source_key].CODE, programCallBackFunction);
                    }
                    // Insert data if item is not exists
                    else {
                            this.Insert(_item, _this.Slug(_item.NAME), programCallBackFunction);
                        }
                }
            } else {
                _this.Init(function () {
                    _this.InsertOrUpdate(data, fields, callBackFunction);
                });
            }
        }
    }]);

    return ManageTable;
}();