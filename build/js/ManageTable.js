class ManageTable {

    constructor(tableName){
        this.tableName = tableName;
        this.loaded = false;
        this.changed = false;
        this.callbackFunction = function () {};
    }

    Init(callbackFunction = function () {}) {
        this.callbackFunction = callbackFunction;
        this.GetTable();
    }

    Query(config){
        if(!config['action']) return console.error('Query action is not set.', config);

        let action = config['action'];

        let param = {
            'IBLOCK_TYPE_ID': 'bitrix_processes',
        };

        let success = function (result) {
            console.log('Success query:', result)
        };

        let error = function (result) {
            alert("Ошибка: " + result.error());
        }

        if(config['IBLOCK_TYPE_ID']) param['IBLOCK_TYPE_ID'] = config['IBLOCK_TYPE_ID'];
        if(config['IBLOCK_CODE']) param['IBLOCK_CODE'] = config['IBLOCK_CODE'];
        if(config['ELEMENT_CODE']) param['ELEMENT_CODE'] = config['ELEMENT_CODE'];
        if(config['LIST_ELEMENT_URL']) param['LIST_ELEMENT_URL'] = config['LIST_ELEMENT_URL'];
        if(config['FIELDS']) param['FIELDS'] = config['FIELDS'];

        if(config['success']) success = config['success'];
        if(config['error']) error = config['error'];

        console.info(action, param);

        BX24.callMethod(
            action,
            param,
            function(result){
                return result.error() ? error(result) : success(result);
            }
        );
    }

    GetTable(){
        let _this = this;
        let name = _this.tableName;

        _this.Query({
            'action': 'lists.get',
            'IBLOCK_CODE': name,
            'success': function (result) {
                // Return alert if table is not exists
                if(!result.data().length) return alert('Таблицы '+name+' не существует.');

                // Save table data
                _this.table = result.data()[0];
                _this.GetFields();
            }
        });
    }

    GetFields(){
        let _this = this;

        let tableCode = _this.table.CODE;

        _this.Query({
            'action': 'lists.field.get',
            'IBLOCK_CODE': tableCode,
            'success': function (result) {
                result = result.data();

                // Return alert if table fields is not exists
                if(!result || !result['NAME']) return alert('Таблица '+_this.table.NAME+' не имеет полей.');

                // Save table fields data
                _this.tableFields = result;
                _this.GetFieldsData(_this.callbackFunction);
                _this.callbackFunction = function () {};
            }
        });
    }

    GetFieldsData(callbackFunction=function(){}){
        let _this = this;

        _this.Query({
            'action': 'lists.element.get',
            'IBLOCK_CODE': _this.tableName,
            'success': function (result) {
                result = result.data();

                // Save table fields data
                _this.sourceFieldsData = result;

                let beautyData = [];
                for(let itemIndex in result){
                    let item = result[itemIndex];

                    beautyData[item['NAME']] = {};
                    beautyData[item['NAME']]['_source_key'] = itemIndex;

                    for(let key in _this.tableFields){
                        // Create element in object by key
                        if(_this.tableFields[key]['MULTIPLE'] == 'N'){

                            // CODE of key
                            let code = _this.tableFields[key].CODE ? _this.tableFields[key].CODE : key;

                            // Save first key from object
                            if(typeof item[key] === 'object'){
                                Object.keys(item[key]).forEach(function (n) {
                                    beautyData[item['NAME']][code] = item[key][n];
                                });
                            }

                            // Just save value
                            else{
                                beautyData[item['NAME']][code] = item[key];
                            }

                        }else{
                            beautyData[item['NAME']][key] = item[key];
                        }

                    }

                }
                _this.beautyFieldsData = beautyData;
                _this.loaded = true;
                _this.changed = false;
                callbackFunction(_this);

                console.log(_this);
            }
        });
    }

    BeautyToSource(item, valueKeys=false){
        let _this = this;
        let sourceItem = {};
        
        // Each item field
        Object.keys(item).forEach(function (itemFieldName) {
            if(itemFieldName == 'NAME'){
                sourceItem.NAME = item.NAME;
            }else{
                // Each source field
                Object.keys(_this.tableFields).forEach(function (property_id) {
                    // Matching CODEs
                    if(_this.tableFields[property_id].CODE == itemFieldName){
                        if(valueKeys){
                            if(!item._source_key) return console.error('Item _source_key not found');

                            let firstKey = typeof _this.sourceFieldsData[item._source_key][property_id] !== 'undefined' ? Object.keys(_this.sourceFieldsData[item._source_key][property_id])[0] : 1;

                            if(!sourceItem[property_id]){
                                sourceItem[property_id] = {};
                            }

                            sourceItem[property_id][firstKey] = item[itemFieldName];
                        }else{
                            sourceItem[property_id] = item[itemFieldName];
                        }

                    }
                });
            }
        });

        return sourceItem;
    }

    Slug(str){
        let letters = [
            ['а', 'a'], ['б', 'b'], ['в', 'v'], ['г', 'g'],
            ['д', 'd'],  ['е', 'e'], ['ё', 'yo'], ['ж', 'zh'], ['з', 'z'],
            ['и', 'i'], ['й', 'y'], ['к', 'k'], ['л', 'l'],
            ['м', 'm'],  ['н', 'n'], ['о', 'o'], ['п', 'p'],  ['р', 'r'],
            ['с', 's'], ['т', 't'], ['у', 'u'], ['ф', 'f'],
            ['х', 'h'],  ['ц', 'c'], ['ч', 'ch'],['ш', 'sh'], ['щ', 'shch'],
            ['ъ', ''],  ['ы', 'y'], ['ь', ''],  ['э', 'e'], ['ю', 'yu'], ['я', 'ya'],

            ['А', 'A'], ['Б', 'B'],  ['В', 'V'], ['Г', 'G'],
            ['Д', 'D'], ['Е', 'E'], ['Ё', 'YO'],  ['Ж', 'ZH'], ['З', 'Z'],
            ['И', 'I'], ['Й', 'Y'],  ['К', 'K'], ['Л', 'L'],
            ['М', 'M'], ['Н', 'N'], ['О', 'O'],  ['П', 'P'],  ['Р', 'R'],
            ['С', 'S'], ['Т', 'T'],  ['У', 'U'], ['Ф', 'F'],
            ['Х', 'H'], ['Ц', 'C'], ['Ч', 'CH'], ['Ш', 'SH'], ['Щ', 'SHCH'],
            ['Ъ', ''],  ['Ы', 'Y'],
            ['Ь', ''],
            ['Э', 'E'],
            ['Ю', 'YU'],
            ['Я', 'YA'],

            ['a', 'a'], ['b', 'b'], ['c', 'c'], ['d', 'd'], ['e', 'e'],
            ['f', 'f'], ['g', 'g'], ['h', 'h'], ['i', 'i'], ['j', 'j'],
            ['k', 'k'], ['l', 'l'], ['m', 'm'], ['n', 'n'], ['o', 'o'],
            ['p', 'p'], ['q', 'q'], ['r', 'r'], ['s', 's'], ['t', 't'],
            ['u', 'u'], ['v', 'v'], ['w', 'w'], ['x', 'x'], ['y', 'y'],
            ['z', 'z'],

            ['A', 'A'], ['B', 'B'], ['C', 'C'], ['D', 'D'],['E', 'E'],
            ['F', 'F'],['G', 'G'],['H', 'H'],['I', 'I'],['J', 'J'],['K', 'K'],
            ['L', 'L'], ['M', 'M'], ['N', 'N'], ['O', 'O'],['P', 'P'],
            ['Q', 'Q'],['R', 'R'],['S', 'S'],['T', 'T'],['U', 'U'],['V', 'V'],
            ['W', 'W'], ['X', 'X'], ['Y', 'Y'], ['Z', 'Z'],

            [' ', '_'],['0', '0'],['1', '1'],['2', '2'],['3', '3'],
            ['4', '4'],['5', '5'],['6', '6'],['7', '7'],['8', '8'],['9', '9'],
            ['-', '-']
        ];
        
        let clearString = '';

        for (let ch of str) {
            var newCh = '';
            for (let j of letters) {
                if (ch == j[0]) {
                    newCh = j[1];
                }
            }
            clearString += newCh;

        }    
            
        return clearString.toString()
            .replace(/\s+/g, '_')           // Replace spaces with -
            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
            .replace(/^-+/, '')             // Trim - from start of text
            .replace(/-+$/, '');            // Trim - from end of text
    }

    Get(searchParams={}){
        let _this = this;
        let data = [];
        let paramsIsEmpty = true;

        if(_this.changed){
            _this.GetFieldsData(function () {
                _this.Get(searchParams);
            });
        }else{
            if(_this.loaded){
                // Each data item
                Object.keys(_this.beautyFieldsData).forEach(function (item) {
                    item = _this.beautyFieldsData[item];

                    let pushed = false;
                    // Each search parameter
                    Object.keys(searchParams).forEach(function (paramIndex) {
                        paramsIsEmpty = false;
                        if(!pushed && (item[paramIndex] == searchParams[paramIndex])){
                            data.push(item);
                            pushed = true;
                        }
                    });
                });

                if(paramsIsEmpty) data = _this.beautyFieldsData;

                return data;
            }else{
                _this.Init(function () {
                    _this.Get(searchParams);
                });
            }
        }
    }

    Insert(item, code, callBackFunction=function(){}){
        let _this = this;

        let source_item = _this.BeautyToSource(item);

        if(_this.loaded){
            return _this.Query({
                'action': 'lists.element.add',
                'IBLOCK_CODE': _this.tableName,
                'ELEMENT_CODE': code,
                'FIELDS': source_item,
                success: function () {
                    callBackFunction(item, 'inserted');
                    _this.changed = true;
                },
                error: function (error) {
                    callBackFunction(item, error.answer.error+': '+error.answer.error_description);
                }
            });
        }else{
            _this.Init(function(){
                _this.Insert(item, code, callBackFunction);
            });
        }
    }

    Update(item, code, callBackFunction=function(){}){
        let _this = this;

        let source_item = _this.BeautyToSource(item, true);

        if(_this.loaded){
            return _this.Query({
                'action': 'lists.element.update',
                'IBLOCK_CODE': _this.tableName,
                'ELEMENT_CODE': code,
                'FIELDS': source_item,
                success: function () {
                    callBackFunction(item, 'updated');
                    _this.changed = true;
                },
                error: function (error) {
                    callBackFunction(item, error.answer.error+': '+error.answer.error_description);
                }
            });
        }else{
            _this.Init(function(){
                _this.Update(item, code, callBackFunction)
            });
        }
    }

    InsertOrUpdate(data, fields, callBackFunction=function(){}){
        let _this = this;

        if(_this.loaded){
            for(let itemKey in data){
                let item = data[itemKey];

                // Formatting search parameters from source
                let searchParams = {};
                for(let field of fields){
                    searchParams[field] = item[field];
                }

                // Check items with current search parameters
                let dbItems = this.Get(searchParams);

                let programCallBackFunction = function (item, status) {
                    return callBackFunction(item, status);
                }

                if(itemKey >= data.length-1){
                    programCallBackFunction = function (item, status) {
                        return _this.GetFieldsData(callBackFunction(item, status));
                    }
                }

                // Update data if items is exists
                if(dbItems.length){
                    item._source_key = dbItems[0]._source_key;
                    this.Update(item, _this.sourceFieldsData[dbItems[0]._source_key].CODE, programCallBackFunction);
                }
                // Insert data if item is not exists
                else{
                    this.Insert(item, _this.Slug(item.NAME), programCallBackFunction);
                }

            }
        }else{
            _this.Init(function(){
                _this.InsertOrUpdate(data, fields, callBackFunction);
            });
        }
    }
}